﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float camSpeedH = 2.0f;
    public float camSpeedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;
    
    void Update()
    {
        //Camera Movement
        yaw += camSpeedH * Input.GetAxis("Mouse X");
        pitch -= camSpeedV * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
    }
}
