﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Camera main;
    public float plSpeed = 5.0f;
    public bool canTeleport = false;
    private Rigidbody rb;
    public GameObject A;
    public GameObject B;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    //void FixedUpdate()
    //{
    //    if (Input.GetKey(KeyCode.W))
    //    {
    //        var forward = main.transform.forward;
    //        forward.y = 0;
    //        rb.velocity = forward.normalized * plSpeed;
    //    }
    //    if (Input.GetKey(KeyCode.S))
    //    {
    //        var backward = -main.transform.forward;
    //        backward.y = 0;
    //        rb.velocity = backward.normalized * plSpeed;
    //    }
    //    if (Input.GetKey(KeyCode.A))
    //    {
    //        var right = -main.transform.right;
    //        right.y = 0;
    //        rb.velocity = right.normalized * plSpeed;
    //    }
    //    if (Input.GetKey(KeyCode.D))
    //    {
    //        var left = main.transform.right;
    //        left.y = 0;
    //        rb.velocity = left.normalized * plSpeed;
    //    }
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        rb.AddForce(Vector3.up * 200);
    //    }
    //}

    public Transform camPivot;
    float heading = 0;
    float footing = 0;
    public Transform cam;

    Vector2 input;

    void Update()
    {
        heading += Input.GetAxis("Mouse X") * Time.deltaTime * 180;
        footing -= Input.GetAxis("Mouse Y") * Time.deltaTime * 180;

        camPivot.rotation = Quaternion.Euler(footing, heading, 0);

        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);

        Vector3 camF = cam.forward;
        Vector3 camR = cam.right;

        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += (camF * (input.y * 2) + camR * (input.x * 2)) * Time.deltaTime * 5;
        }
        else
            transform.position += (camF * input.y + camR * input.x) * Time.deltaTime * 5;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * 200);
        }
    }
    //Code for teleporting. 
        //gameObject.transform.position = B.transform.position + new Vector3(0, B.transform.position.y + 1, 0);
        //main.transform.rotation = new Quaternion(0, 0, 0, 1);
}
