﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement2D : MonoBehaviour
{

    [Header("Player Settings")]
    public int PlayerSpeed;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Player Movement *NO VERTICAL MOVEMENT//ADD JUMP MOVEMENT
        float hMove = Input.GetAxis("HJoy1");

        if (hMove >= 0.1f)
        {
            rb.AddForce(transform.right * PlayerSpeed);
        }
        if (hMove <= -0.1f)
        {
            rb.AddForce(-transform.right * PlayerSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(transform.right * PlayerSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-transform.right * PlayerSpeed);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(0,200,0);
        }
    }
}
