﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Interaction : MonoBehaviour
{
    Shader shader1;
    Shader shader2;
    Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        shader1 = Shader.Find("LightWeight Render Pipeline/Lit");
        shader2 = Shader.Find("Unlit/Outline");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.tag == "Door")
        {
            if (rend.material.shader == shader1)
            {
                rend.material.shader = shader2;
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene("Trust");
            }
        }
        
    }
    private void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Door")
        {
            if (rend.material.shader == shader2)
            {
                rend.material.shader = shader1;
            }
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Kill")
        {
             SceneManager.LoadScene("Trust");
        }
        if (other.gameObject.tag == "Destroy")
        {
            Destroy(GameObject.FindGameObjectWithTag("ToDestroy"));
        }
    }
}
