﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class OrbController : MonoBehaviour
{
    public Vector3 rot;
    public GameObject target;

    public OrbType orbType;

    public float speed;

    // Update is called once per frame
    void Update()
    {
        Orbit();

        for (int i = 0; i < 7; i++)
        {
            if (ProgressionTracker.instance.LC[i])
            {
                DefaultPosition((OrbType)i);
                ProgressionTracker.instance.LC[i] = false;
            }
        }
    }
    public void Orbit()
    {
        if (ProgressionTracker.instance.canRot[(int)orbType])
        {
            ProgressionTracker.instance.orbs[(int)orbType].transform.RotateAround(target.transform.position, rot, speed * Time.deltaTime);
        }
    }
    public void DefaultPosition(OrbType orbType)
    {
        ProgressionTracker.instance.orbs[(int)orbType].transform.DOLocalMove(new Vector3(0f, 2f + 2f * (int)orbType, 2.5f), 2.5f).SetEase(Ease.InOutCubic);
        ProgressionTracker.instance.canRot[(int)orbType] = false;
    }
}