﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCharacterController : MonoBehaviour
{
    private string moveInputAxis = "VJoy1";
    private string turnInputAxis = "HJoy1";

    public float rotationRate = 360;

    public float moveSpeed = 2;

    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        float moveAxis = 0;
        float turnAxis = 0;

        if (Input.GetAxisRaw("HJoy1") > 0.25f || Input.GetAxisRaw("HJoy1") < -0.25f || Input.GetAxisRaw("VJoy1") > 0.25f || Input.GetAxisRaw("VJoy1") < -0.25f)
        {
            anim.SetBool("Walking", true);
            moveAxis = Input.GetAxisRaw(moveInputAxis);
            turnAxis = Input.GetAxisRaw(turnInputAxis);
        }
        else
        {
            anim.SetBool("Walking", false);
        }
        ApplyInput(moveAxis, turnAxis);
    }
    
    private void ApplyInput(float moveInput, float turnInput)
    {
        Move(moveInput);
        Turn(turnInput);
    }
    private void Move(float input)
    {

        transform.Translate(Vector3.forward * input * moveSpeed * Time.deltaTime);
    }
    private void Turn(float input)
    {
        transform.Rotate(0, input * rotationRate * Time.deltaTime, 0);
    }

}
