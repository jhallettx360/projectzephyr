﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCameraController : MonoBehaviour
{
    public Transform target;
    public float lookSmooth = 0.09f;
    public Vector3 offsetFromTarget = new Vector3(0, 2, -1);
    public float xTilt = 10;

    Vector3 destination = Vector3.zero;
    TestCharacterController charController;
    float rotateVel = 0;

    // Start is called before the first frame update
    void Start()
    {
        SetCameraTarget(target);
    }

    void SetCameraTarget(Transform t)
    {
        target = t;

        if (target != null)
        {
            if (target.GetComponent<TestCharacterController>())
            {
                charController = target.GetComponent<TestCharacterController>();
            }
            else
                Debug.LogError("The camera's target needs a character controller.");
        }
        else
            Debug.LogError("Your camera needs a target.");
    }

    private void LateUpdate()
    {
        MoveToTarget();
        LookAtTarget();
    }
    void MoveToTarget()
    {
        //destination = charController.TargetRotation * offsetFromTarget;
        destination += target.position;
        transform.position = destination;
    }
    void LookAtTarget()
    {
        float eulerYAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y, ref rotateVel, lookSmooth);
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, eulerYAngle, 0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
