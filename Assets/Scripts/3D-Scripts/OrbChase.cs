﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class OrbChase : MonoBehaviour
{
    public Transform target1, target2, target3, target4, target5;
    public Vector3 pos1, pos2, pos3, pos4, pos5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if ((gameObject.transform != target1) || gameObject.transform != target2 || gameObject.transform != target3 || gameObject.transform != target4 || gameObject.transform != target5)
            {
                transform.DOLocalMove(pos1, 2.5f).SetEase(Ease.InOutCubic);
            }
            if (gameObject.transform.position == pos1)
            {
                transform.DOLocalMove(pos2, 2.5f).SetEase(Ease.InOutCubic);
            }
            if (gameObject.transform.position == pos2)
            {
                transform.DOLocalMove(pos3, 2.5f).SetEase(Ease.InOutCubic);
            }
            if (gameObject.transform.position == pos3)
            {
                transform.DOLocalMove(pos4, 2.5f).SetEase(Ease.InOutCubic);
            }
            if (gameObject.transform.position == pos4)
            {
                transform.DOLocalMove(pos5, 2.5f).SetEase(Ease.InOutCubic);
            }
            if (gameObject.transform.position == pos5)
            {
                transform.DOLocalMove(pos5, 0.1f).SetEase(Ease.InOutCubic);
            }
        }
    }
}
