﻿public enum OrbType
{
    Fear,
    Guilt,
    Shame,
    Grief,
    Lies,
    Illusion,
    Earth
};