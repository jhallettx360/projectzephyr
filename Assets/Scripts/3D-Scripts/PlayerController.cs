﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject pauseMenu, Atext1;
    public GameObject Atext2;
    private string moveInputAxis = "VJoy1";
    private string turnInputAxis = "HJoy1";

    public float rotationRate = 360;

    public float moveSpeed = 2;

    private string currentScene;

    Animator anim;

    private void Start()
    {
        pauseMenu = GameObject.Find("Player/Main Camera/PauseMenu");
        Atext1 = GameObject.Find("Shrines/altar_sigil_table_v01/A");
        Atext2 = GameObject.Find("altar_sigil_table_v01/A1");
        currentScene = SceneManager.GetActiveScene().name;
        if (currentScene == "Fear")
        {
            Atext2.SetActive(false);
        }
        anim = GetComponent<Animator>();
        
    }
    private void Update()
    {
        if (pauseMenu.activeSelf == false)
        {
            float moveAxis = 0;
            float turnAxis = 0;

            if (Input.GetAxisRaw("HJoy1") > 0.25f || Input.GetAxisRaw("HJoy1") < -0.25f || Input.GetAxisRaw("VJoy1") > 0.25f || Input.GetAxisRaw("VJoy1") < -0.25f)
            {
                anim.SetBool("Walking", true);
                moveAxis = Input.GetAxisRaw(moveInputAxis);
                turnAxis = Input.GetAxisRaw(turnInputAxis);
            }
            else if (((Input.GetAxisRaw("HJoy1") < 0.25f) && (Input.GetAxisRaw("HJoy1") > -0.25f)) && (Input.GetAxisRaw("VJoy1") < 0.25f && (Input.GetAxisRaw("VJoy1") > -0.25f)))
            {
                anim.SetBool("Walking", false);
            }
            ApplyInput(moveAxis, turnAxis);
        }
    }
    private void ApplyInput(float moveInput, float turnInput)
    {
        Move(moveInput);
        Turn(turnInput);
    }
    private void Move(float input)
    {
        transform.Translate(Vector3.forward * input * moveSpeed * Time.deltaTime);
    }
    private void Turn(float input)
    {
        transform.Rotate(0, input * rotationRate * Time.deltaTime, 0);
    }
    private void ObjectRotation()
    {
        Atext1.transform.LookAt(this.gameObject.transform);
        Atext2.transform.LookAt(this.gameObject.transform);
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Fear")
        {
            Atext1.SetActive(true);
        }
        if (other.gameObject.tag == "Goal")
        {
            Atext2.SetActive(true);
        }
        if (Input.GetButtonDown("A") || (Input.GetKeyDown(KeyCode.Return)))
        {
            if (other.gameObject.tag == "Fear")
            {
                anim.SetBool("Meditating", true);
                StartCoroutine(DelayLoadLvl(3.0f));
            }
            if (other.gameObject.tag == "Goal")
            {
                anim.SetBool("Meditating", true);
                ProgressionTracker.instance.CompleteLevel();
                StartCoroutine(DelayLoadHub(3.0f));
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Fear")
        {
            Atext1.SetActive(false);
        }
        if (other.gameObject.tag == "Goal")
        {
            Atext2.SetActive(false);
        }
    }
    IEnumerator DelayLoadLvl(float waitTime)
    {
        Debug.Log("Entered Loop");
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("Fear");
    }
    IEnumerator DelayLoadHub(float waitTIme)
    {
        yield return new WaitForSeconds(waitTIme);
        SceneManager.LoadScene("Hub");
    }
}
