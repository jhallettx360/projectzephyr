﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NPCFade : MonoBehaviour
{
    public MeshRenderer rend;
    public Transform target;

    private void Start()
    {
        rend = GetComponent<MeshRenderer>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            rend.material.DOColor(new Color(1,0,0,0), "_BaseColor", 2.0f).SetEase(Ease.InOutCubic);
        }
        //StartCoroutine(RotateAndRun(5.0f));
        StartCoroutine(DestroyNPC(1.5f));
    }
    IEnumerator DestroyNPC(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(this.gameObject);
    }
    IEnumerator RotateAndRun(float waitTime)
    {
        transform.DOLocalRotate(target.transform.position - transform.position, 2.0f).SetEase(Ease.InOutCubic);
        yield return new WaitForSeconds(waitTime);
    }
}