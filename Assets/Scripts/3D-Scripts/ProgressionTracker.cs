﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ProgressionTracker : MonoBehaviour
{
    public static ProgressionTracker instance;

    public bool[] LC = new bool[7];
    public OrbType currentOrbType;

    public GameObject[] orbs;
    public bool[] canRot;

    // Start is called before the first frame update
    void Awake()
    {
        // Destroy duplicate object - "The Singleton Pattern"
        if (instance != null)
        {
            instance.orbs = orbs;
            Destroy(this.gameObject);
            return;
        }
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadHub()
    {
        SceneManager.LoadScene("Hub");
    }
    public void CompleteLevel()
    {
        LC[(int)currentOrbType] = true;
        currentOrbType = (OrbType)((int)currentOrbType + 1);
    }
}