﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject target;    
    public float turningSpeed = 60.0f;
    public float timestamp = 60.0f;
    private float horCamMov, verCamMov;

    //TEST VARIABLES
    private const float Y_ANGLE_MIN = 5.0f;
    private const float Y_ANGLE_MAX = 45.0f;

    //public Transform player;

    private float distance = -5.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        pauseMenu = GameObject.Find("Player/Main Camera/PauseMenu");
    }
    // Update is called once per frame
    void Update()
    {
        if (pauseMenu.activeSelf == false)
        {
            //Camera Movement -CURRENT
            horCamMov = Input.GetAxis("HJoy2") * turningSpeed * Time.deltaTime;
            transform.RotateAround(target.transform.position, Vector3.up, horCamMov);

            if ((Input.GetAxis("HJoy2") > 0.25f) || (Input.GetAxis("HJoy2") < -0.25f) || (Input.GetAxis("VJoy2") > 0.25f) || (Input.GetAxis("VJoy2") < -0.25f))
            {
                timestamp = Time.time + 60.0f;
            }
            verCamMov = Input.GetAxis("VJoy2") * turningSpeed * Time.deltaTime;
            transform.Rotate(verCamMov, 0, 0);

            //Reset Camera Position - CURRENT
            if ((Time.time >= timestamp && timestamp != 0) || (Input.GetButton("RClick")))
            {
                timestamp = 0.0f;
                transform.DOLocalMove(new Vector3(1.32f, 1.821274f, -3.54f), 1.5f).SetEase(Ease.InOutCubic);
                transform.DOLocalRotate(new Vector3(7.772f, 0.0f, 0.0f), 1.5f).SetEase(Ease.InOutCubic);
            }
        }
        if (Input.GetButtonDown("Start"))
        {
            if (pauseMenu.activeSelf == false)
            {
                pauseMenu.gameObject.SetActive(true);
            }
            else
            {
                pauseMenu.gameObject.SetActive(false);
            }
        }
    }
}
